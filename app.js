let submit = document.querySelector(".submit");
let name = document.querySelector('.name');

function get_weather(city) {
    let weather_api_url = "http://api.openweathermap.org/data/2.5/weather?q=" + city + "&appid=6bc229d60a3ea09cdd54e62fbf03499e";

    const weather_data = {};

    fetch(weather_api_url)
        .then(response => response.json())
        .then(data => {
            weather_data["name"] = data['name'];
            weather_data["temp"]=data['main']['temp'];
            weather_data["country"]=data['sys']['country'];
            weather_data["description"]=data['weather'][0]['description'];
            weather_data["humidity"]=data['main']['humidity'];
        })
    return weather_data
}

let to_Celcuius = (temp) => Math.round(temp-273) +" °C";

submit.addEventListener("click", (e) => {
    let city = document.querySelector('.input_city').value;
    let data = get_weather(city);
    setTimeout(() => {

        let name = document.querySelector('.name');
        let temp = document.querySelector(".temp");
        let country=document.querySelector(".country");
        let desc =document.querySelector(".desc");
        let humidity =document.querySelector(".humidity");


        name.innerHTML = "Weather in " + data['name'];
        country.innerHTML = "Country: "+ data['country'];
        temp.innerHTML = "Temperature: "+ to_Celcuius(data['temp']);
        desc.innerHTML = " Description: "+data['description'];
        humidity.innerHTML = "Humidity: "+data['humidity']+"%";
    }, 2000);
})
